from datetime import timedelta

from django.utils import timezone

from common.models import User
from izufang import app


@app.task
def check_inactive_users():
    last_30_days = timezone.now() - timedelta(days=30)
    User.objects.filter(lastvisit__lt=last_30_days).update(is_active=False)
